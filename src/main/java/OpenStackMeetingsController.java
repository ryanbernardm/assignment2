
import java.io.IOException;


import org.jsoup.select.Elements;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class OpenStackMeetingsController {	


	public OpenStackMeetingsController(){
		
	}
	
	private QueryService queryService;

	public OpenStackMeetingsController(QueryService queryService){
		this.queryService = queryService;
	}
	
	@ResponseBody
	@RequestMapping("/")
	public String printWelcome(){
		return "Welcome to OpenStack meeting statistics calculation page. Please provide project and year as query parameters.";
	}
	
	
	@ResponseBody
	@RequestMapping("/openstackmeetings")
	public String handler(@RequestParam("year") String year, @RequestParam("project") String project)
			throws IOException {
		
		String response = "";
		Elements validProject;
		Elements validYear;
		int numFiles;
		
		//Validate if project is on eavesdrop
		validProject = this.queryService.validateProject("http://eavesdrop.openstack.org/meetings", project.toLowerCase());
		if(validProject == null || validProject.isEmpty() || project == ""){
			response += "Project with <"+project+"> not found\n";
			if(!isNumInRange(year)){
				response += "Invalid year <" + year + "> for project <" + project + ">\n";
			}
			return response;
		}
		
		//Make sure year parameter is possible before calling validateYear with faulty url
		if(!isNumInRange(year)){
			return "Invalid year <" + year + "> for project <" + project + ">\n";
			
		}
		
		//Call service class to ensure year is valid for project name
		validYear = this.queryService.validateYear(validProject, year);
		if(validProject == null || validProject.isEmpty()){
			response += "Invalid year <" + year + "> for project <" + project + ">\n";
			return response;
		}
		
		numFiles = queryService.getNumFiles(validYear, project);
		response += "Number of meeting files: "+ numFiles;
		return response;
	}
	
	//Checks if number only contains digits and if in range
	public boolean isNumInRange(String year)  
	{  
	  try  
	  {  
	    double num = Double.parseDouble(year);
	    if(num > 2017 || num < 1000){
	    	return false;
	    }
	  }  
	  catch(NumberFormatException a)  
	  {  
	    return false;  
	  }  
	  return true;  
	}
	
}
