

import java.io.IOException;

import org.jsoup.select.Elements;

public interface QueryService {

	public Elements validateProject(String url, String project) throws IOException;
	public Elements validateYear(Elements links, String project) throws IOException;
	public int getNumFiles(Elements links, String project) throws IOException;
	
}
