

import java.io.IOException;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

public class QueryServiceImpl implements QueryService{


	public QueryServiceImpl() throws IOException{
		
	}

	//Checks if project is on eavesdrop
	@Override
	public Elements validateProject(String url, String project) throws IOException {
		Document document = Jsoup.connect(url).get();
		Elements links = document.getElementsByAttributeValue("href", project + "/");
		return links;
	}

	//Checks if file exists with year parameter for this given project
	@Override
	public Elements validateYear(Elements links, String year) throws IOException {
		Document document = Jsoup.connect(links.attr("abs:href")).get();
		links = document.getElementsByAttributeValue("href", year + "/");
		return links;
	}

	//Checks number of files for given project and year
	@Override
	public int getNumFiles(Elements links, String project) throws IOException{
		int numFiles = 0;
		Document document = Jsoup.connect(links.attr("abs:href")).get();
		links = document.getElementsByAttributeValueStarting("href", project);
		if(links != null && !links.isEmpty()){
			numFiles = links.size();
		}
		return numFiles;
	}
}
