
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import java.io.IOException;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;


public class TestOpenStackMeetingsController {
	
	OpenStackMeetingsController controller;
	QueryService mockQuery = null;
	
	@Before
	public void setUp1(){
		mockQuery = mock(QueryService.class);		
		controller = new OpenStackMeetingsController(mockQuery);
	}
	
	@Test
	public void testWelcome(){
		assertEquals("Welcome to OpenStack meeting statistics calculation page. Please provide project and year as query parameters.", controller.printWelcome());
	}
	
	@Test
	public void testHandlerWithInvalidYear() throws IOException{
		Document document = Jsoup.connect("http://eavesdrop.openstack.org/meetings").get();
		Elements links = document.getElementsByAttributeValue("href", "solum/");
		when(mockQuery.validateProject("http://eavesdrop.openstack.org/meetings", "solum")).thenReturn(links);
		
		String response = controller.handler("999", "solum");
		
		assertEquals(response, "Invalid year <999> for project <solum>\n");
		
		verify(mockQuery).validateProject("http://eavesdrop.openstack.org/meetings", "solum");
	}
	
	@Test
	public void testHandlerWithInvalidProjectAndValidYear() throws IOException{
		Document document = Jsoup.connect("http://eavesdrop.openstack.org/meetings").get();
		Elements links = document.getElementsByAttributeValue("href", "notAProject/");
		when(mockQuery.validateProject("http://eavesdrop.openstack.org/meetings", "notaproject")).thenReturn(links);
		
		String response = controller.handler("2015", "notAProject");
		
		assertEquals(response, "Project with <notAProject> not found\n");
		
		verify(mockQuery).validateProject("http://eavesdrop.openstack.org/meetings", "notaproject");
	}
	
	
	@Test
	public void testHandlerWithInvalidProjectAndInvalidYear() throws IOException{
		Document document = Jsoup.connect("http://eavesdrop.openstack.org/meetings").get();
		Elements links = document.getElementsByAttributeValue("href", "notAProjectAgain/");
		when(mockQuery.validateProject("http://eavesdrop.openstack.org/meetings", "notaprojectagain")).thenReturn(links);
		
		String response = controller.handler("notAYear", "notAProjectAgain");
		
		assertEquals(response, "Project with <notAProjectAgain> not found\nInvalid year <notAYear> for project <notAProjectAgain>\n");
		
		verify(mockQuery).validateProject("http://eavesdrop.openstack.org/meetings", "notaprojectagain");
	}
	
	@Test
	public void testHandlerWithValidProjectAndYear() throws IOException{
		Document document = Jsoup.connect("http://eavesdrop.openstack.org/meetings").get();
		Elements links = document.getElementsByAttributeValue("href", "solum/");
		when(mockQuery.validateProject("http://eavesdrop.openstack.org/meetings", "solum")).thenReturn(links);
		
		document = Jsoup.connect(links.attr("abs:href")).get();
		Elements links2 = document.getElementsByAttributeValue("href", "2015/");
		when(mockQuery.validateYear(links, "2015")).thenReturn(links2);
		
		when(mockQuery.getNumFiles(links2, "solum")).thenReturn(4);
		
		String response = controller.handler("2015", "solum");
		
		assertEquals(response, "Number of meeting files: 4");
		
		verify(mockQuery).validateProject("http://eavesdrop.openstack.org/meetings", "solum");
		verify(mockQuery).validateYear(links, "2015");
		verify(mockQuery).getNumFiles(links2, "solum");
	}
	
	@Test
	public void testIsNumInRange() throws IOException{
		String num = "asdfeee777";
		assertEquals(false, controller.isNumInRange(num));
	}

	
}
